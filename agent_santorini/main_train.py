# -*- coding: utf-8 -*-

import sys
import os
import torch 
import torch.optim as optim
import numpy as np
import matplotlib.pyplot as plt
import json
import copy
sys.path.insert(1, 'd01_agent/')
sys.path.insert(2, 'd00_utils/')
# torch.autograd.set_detect_anomaly(True)
from agent_util import AgentUtil
from a2c_santorini import SantoriniActor, SantoriniCritic

import gym

env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
     if 'santorini' in env:
          print('Remove {} from registry'.format(env))
          del gym.envs.registration.registry.env_specs[env]
          
import gym_santorini

# will want to add parallel environments later
# lets try to train on just one instance right now
# sys.path.insert(1, 'd00_utils/')
# from parallel_env import ParallelEnv
# n_train_processes = 3 

def tensor_converter(numpy_array): return torch.from_numpy(numpy_array).float()


#start here tomorrow
# need to load train_config.json and create system to save data
# file name will be agent_actor_name_iteration#.pt



load_path = ''
load_file_actor = ''
load_file_critic = ''

save_path =''
save_file_actor = ''
save_file_critic = ''

train_iteration = None
train_save_interval = None

learning_rate = None
gamma = None

json_path = "d06_config/train_config.json"
with open(json_path) as f:
    data = json.load(f)
    
    load_path = data['load_path']
    load_file_actor = data['load_file_actor']
    load_file_critic = data['load_file_critic']
    
    
    save_path =  data['save_path']
    save_file_actor = data['save_file_actor']
    save_file_critic = data['save_file_critic']
    
    train_iteration =  data['train_iteration']
    train_save_interval = data['train_save_interval']

    gamma = data['gamma']
    learning_rate = data['learning_rate']

# todo list

# saving /loading weights (completed) XXX

# need a elo calculator in agent_util

# basically a testing scenario combat weights v weights function
     # loads 1st weight vs loads second weight
     # code that decides which is white and which is black
     # weight 1, player swap, weight 2, player swap, etc...

# player v computer function
     # loads 1st weight
     # custom function that requires input value from human
     # weight 1 decides its own moves after human move
     
# graphing tool to see learning rate/trajectory
     # compares previous iterations
     # winrates of previous iterations

# heat map of value function
     # history of value function change over time?

# heat map of policy function 
     # history of policy function change over time?
def load_model( torch_object, load_path):
    return torch_object.load_state_dict(torch.load(load_path))
    
def save_model(torch_object, file_name):
    return torch.save(torch_object.state_dict(), save_path+file_name)

def main_train():
    
    agent_utility = AgentUtil()
    agent_actor = SantoriniActor()
    agent_critic = SantoriniCritic()

    agent_actor.load_state_dict(torch.load(load_path + load_file_actor))
    agent_critic.load_state_dict(torch.load(load_path + load_file_critic))

    
    environment = gym.make('santorini-v0')
    agent_actor_optimizer = optim.Adam(agent_actor.parameters(), lr=learning_rate) 
    agent_critic_optimizer = optim.Adam(agent_critic.parameters(), lr=learning_rate)

    # initial run
    environment.create_board_custom([[6, 8], [16,18]])
    
    episode_rewards = []
    for i in range(1, train_iteration+1):

        current_board_state = environment.reset()
        current_board_state =np.expand_dims(current_board_state, axis = 0)
        current_board_state = np.expand_dims(current_board_state, axis = 0)
        done = False
        total_reward = 0
        
        while not done:
            
            # this action probability needs to be reworked to only be calculating the 'legal moves'
            action_probability = agent_actor.forward(tensor_converter(current_board_state))       
            
            # links:
                #https://tabletopia.com/playground/players/id1561552/1zu5n3/play
                #https://github.com/miki998/3d_convolution_neural_net_MNET/blob/master/pytorch_model.py
                #https://lilianweng.github.io/lil-log/2018/04/08/policy-gradient-algorithms.html#policy-gradient
                #https://github.com/hermesdt/reinforcement-learning/blob/master/a2c/cartpole_a2c_online.ipynb
                #https://pytorch.org/docs/master/generated/torch.nn.Conv3d.html
                #https://pytorch.org/docs/stable/generated/torch.nn.Conv2d.html#torch.nn.Conv2d
            
                #https://www.yongfengli.tk/2018/04/13/inplace-operation-in-pytorch.html


            ################################## FUTURE REFACTOR NEEDED I DO NOT LIKE :( #############################################

            # reshapes the torch to the same shape as the output state
            action_probability_reshape =  torch.reshape(action_probability, (3,64,5,5))
            # creates a numpy version to calculate the board state softmax according to the legal moves
            action_probability_numpy = copy.deepcopy(action_probability_reshape.detach().numpy())
           


            # generates a move_dictionary that contains all the valid moves
            move_dictionary = agent_utility.generate_move_dictionary(environment.check_valid_moves()[1], action_probability_numpy)
            
            # generates the softmax results along with the move dictionary
            softmax_result, move_dictionary = agent_utility.generate_move_softmax(move_dictionary)
            
            # sets the tensor value to zero
            action_probability_zero = action_probability_reshape.clone()[action_probability_reshape != 0] - action_probability_reshape.clone()[action_probability_reshape != 0]
            
            # for some reason the dimensions gets fucked and this is recorrecting the dimensions
            action_probability_zero = action_probability_zero.unsqueeze(0)
            
            # reshaping it back to the output board state
            action_probability_zero_reshape =  torch.reshape(action_probability_zero, (3,64,5,5))

            
            # creating a result tensor with all output values sorted by dictionary
            softmax_result_tensor = torch.FloatTensor(softmax_result)
            
            
            # creates a zero tensor to add to the output tensor
            softmax_tensor = torch.zeros(3,64,5,5)

            # assigns softmax tensor values from the result tensor in accordance to the dictionary move values
            c = 0
            for move in move_dictionary:
                softmax_tensor[move[1][0], move[1][1], move[1][2], move[1][3]] = softmax_result_tensor[c]
                c += 1
                
            # sums the zero tensor with the softmax tensor to create the 'correct' output tensor according to all legal moves
            output_tensor = action_probability_zero_reshape + softmax_tensor
            
            # come back and change this for loop to create the matrix
            # then add the matrix to action_probability_zero_reshape
        

            # selects a move from the possible moves via softmax
            action_selected = agent_utility.select_move(softmax_result, move_dictionary)
            # converts selection to torch tensor value to compute for backward gradient
            action_selected_torch = action_probability_reshape[action_selected[1][0],action_selected[1][1],action_selected[1][2],action_selected[1][3]]
            

            # reconverts the dimension from 3,64,5,5 back to 1,4800
            output_tensor = output_tensor.flatten()
            output_tensor = output_tensor.unsqueeze(0)
            # categorizes the values
            dist = torch.distributions.Categorical(probs = output_tensor)
            

            ################################## FUTURE REFACTOR NEEDED I DO NOT LIKE :( #############################################
            # print(action_selected)
            # print(action_selected_torch)
            # print(dist)
            # print(dist.log_prob(action_selected_torch))
            # print(dist.log_prob(dist.sample()))
            
            
            next_board_state, reward, done= environment.step(action_selected[1])
            
            
            # print(next_board_state)
            # print(reward)
            # print(1-done)
            
            next_board_state = np.expand_dims(next_board_state, axis = 0)
            next_board_state = np.expand_dims(next_board_state, axis = 0)


            # start here after you come back
            # there needs to be an if conditional to check if opponent can or cannot move
            # if opponent cannot move then game is over and the opponent will lose
            # create a temporary player swap to check and see if the other player can or cannot move
            environment.player_swap()
            if (len (environment.check_valid_moves()[1]) == 0): 
                done = True
            environment.player_swap()
            # print(agent_critic(tensor_converter(next_board_state)))
            # print(agent_critic(tensor_converter(current_board_state)))    
            advantage = reward + (1-done) * gamma * agent_critic(tensor_converter(next_board_state)) - agent_critic(tensor_converter(current_board_state))
            
            total_reward += reward
            # current_board_state = next_board_state
            
            critic_loss = advantage.pow(2).mean()
            agent_critic_optimizer.zero_grad()
            critic_loss.backward()
            agent_critic_optimizer.step()
            
            actor_loss = -dist.log_prob(action_selected_torch)*advantage.detach()
            agent_actor_optimizer.zero_grad()
            actor_loss.backward()
            agent_actor_optimizer.step()
            
            
            #write the player swap here :) 
            environment.change_player_turn()
            environment.player_swap()
            current_board_state = environment.get_board_state()
            
            current_board_state = np.expand_dims(current_board_state, axis = 0)
            current_board_state = np.expand_dims(current_board_state, axis = 0)
            # there exist a scenario where a player can move and after swapping the other player cannot move
            # this is a quick fix to address that issue
            # game will end in this situation :\
                            
        if (i % train_save_interval == 0):
            print("Finished: " + str(i))
            save_model(agent_actor, save_file_actor + str(i) + '.pt')
            save_model(agent_critic, save_file_critic + str(i) + '.pt')
        print(i)
    # environment.print_board_state()
    # environment.print_player_position()
    # environment.print_player_init_position()
    # environment.print_move_logger()
if __name__ == "__main__":
    main_train()
