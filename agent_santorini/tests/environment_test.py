# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-

import gym

# resets the santorini so the re-register error does not pop up
env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
     if 'santorini' in env:
          print('Remove {} from registry'.format(env))
          del gym.envs.registration.registry.env_specs[env]
          
import gym_santorini

def test_gym_setup():
    print("start gym setup test")
    test = gym.make('santorini-v0')
    print('end gym setup test')
    
    
def test_board_random_setup():
    print('start board random generation test')
    test = gym.make('santorini-v0')
    test.create_board_random()
    # ensure there are only two values in the first and second array
    # ensure there are no values in the 3rd to 6th array
    print('print board state')
    test.print_board_state()
    
    # ensure move logger values match board state value
    # value // 5 = y axis coordinate, value % 5 = x axis coordinate
    # positive y axis goes from up to down
    # positive x axis goes from left to right
    print('print move logger')
    test.print_move_logger()
    
    # ensure that values here are correct compared to the board/move logger
    print('print init player position')
    test.print_player_init_position()
    
    # ensure that values here match player init position values
    print('print player position')
    test.print_player_position()
    
    print('end board random generation test')

def test_board_random_setup_2_moves():
    print('start board random setup 2 moves test')
    
    test = gym.make('santorini-v0')
    test.create_board_random()
    
    # both players take turns moving once
    test.step(test.check_valid_moves()[1][0])
    test.player_swap()
    test.step(test.check_valid_moves()[1][0])
    test.player_swap()
    
    # make sure there are only 2 values for the first and second arra
    # for the 3rd to 6th array make sure there are only two values in total
    test.print_board_state()
    # make sure the move logger contains all valid moves that lead to the board state shown above
    test.print_move_logger()
    # make sure the player current position matches end of logger position
    test.print_player_position()
    # make sure the player init position matches start of logger position
    test.print_player_init_position()
    print('end board random setup 2 moves test')

def test_board_random_setup_4_moves_reset():
    print('start board random setup 2 moves reset test')
    
    test = gym.make('santorini-v0')
    test.create_board_random()
    
     # both players take turns moving once
    test.step(test.check_valid_moves()[1][0])
    test.player_swap()
    test.step(test.check_valid_moves()[1][0])
    test.player_swap()
    test.step(test.check_valid_moves()[1][0])
    test.player_swap()
    test.step(test.check_valid_moves()[1][0])
    test.player_swap()
    print('before reset')
    # board state after 4 moves
    test.print_board_state()
    # ensure there are 6 elements in the move logger
    test.print_move_logger()
    # ensure that player position matches logger display
    test.print_player_position()
    # ensure that player init position matches the first two elements in the move logger
    test.print_player_init_position()
    
    test.reset()
    
    print('after reset')
    # ensure that board state reset to original
    # ensure there are only 2 values in the first array
    # ensure there are only 2 values in the second array
    # ensure there are no values in the 3rd to 6th array
    test.print_board_state()
    # ensure that move logger is reset to having two elements
    test.print_move_logger()
    # ensure that player position matches player init position
    test.print_player_position()
    # ensure that player init position matches logger data
    test.print_player_init_position()
    print('end board random setup 2 moves reset test')

def test_board_custom_setup():
    print('start board custom generation test')
    test = gym.make('santorini-v0')
    test.create_board_custom([[16, 0], [11, 12]])
                              

    print('print board state')

    # 1st array at [0,0] and [3,1]
    # 2nd array at [2,1] and [2,2]
    # no values from 3rd to 6th array
    test.print_board_state()

    # ensure move logger values match board state value
    # value // 5 = y axis coordinate, value % 5 = x axis coordinate
    # positive y axis goes from up to down
    # positive x axis goes from left to right
    print('print move logger')
    test.print_move_logger()
    
    # ensure that values here are correct compared to the board/move logger
    print('print init player position')
    test.print_player_init_position()
    
    # ensure that values here match player init position values
    print('print player position')
    test.print_player_position()
    
    print('end board custom generation test')


def test_board_custom_setup_4_step():
    print('start board custom setup 4 moves test')
    
    test = gym.make('santorini-v0')
    test.create_board_custom([[16, 0], [11, 12], [18, 0], [1, 11], [24, 1], [2, 6]])
    
    # make sure there are only 2 values for the first and second array
    # for the 3rd to 6th array make sure there are only 4 values in total
    # in this instance this will build a tower at coordinate 0,3
    test.print_board_state()
    # make sure the move logger contains all valid moves that lead to the board state shown above
    test.print_move_logger()
    # make sure the player current position matches end of logger position
    test.print_player_position()
    # make sure the player init position matches start of logger position
    test.print_player_init_position()
    print('end board custom setup 4 moves test')

def test_board_random_setup_6_moves_reset():
    print('start board custom setup 6 moves reset test')
    
    test = gym.make('santorini-v0')
    test.create_board_custom([[16, 0], [11, 12], [18, 0], [1, 11], [24, 1], [2, 6]])
    test.step(test.check_valid_moves()[1][0])
    test.player_swap()
    test.step(test.check_valid_moves()[1][0])
    test.player_swap()
    
     # both players take turns moving twice after the initial logger
    print('before reset')
    # board state after 6 totals moves (2 computer generated moves)
    test.print_board_state()
    # ensure there are 8 elements in the move logger
    test.print_move_logger()
    # ensure that player position matches logger display
    test.print_player_position()
    # ensure that player init position matches the first two elements in the move logger
    test.print_player_init_position()
    
    test.reset()
    
    print('after reset')
    # ensure that board state reset to original logger
    # ensure there are only 2 values in the first array
    # ensure there are only 2 values in the second array
    # ensure there are 4 values in the 3rd to 6th array
    test.print_board_state()
    # ensure that move logger is reset to having 6 elements
    test.print_move_logger()
    # ensure that player position matches player position after input move lgoger
    test.print_player_position()
    # ensure that player init position matches beginninng of logger data
    test.print_player_init_position()
    print('end board custom setup 6 moves reset test')
# for testing the reset function in santorini
def environment_test():
    
    # test_gym_setup()
    # test_board_random_setup()
    # test_board_random_setup_2_moves()
    # test_board_random_setup_4_moves_reset()
    # test_board_custom_setup()
    # test_board_custom_setup_4_step()
    # test_board_random_setup_6_moves_reset()
    return None
    

if __name__ == "__main__":
    environment_test()

