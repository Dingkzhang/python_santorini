# -*- coding: utf-8 -*-
import sys
import os
import torch 
import torch.optim as optim
import numpy as np
import matplotlib.pyplot as plt

sys.path.insert(1, 'd01_agent/')
sys.path.insert(2, 'd00_utils/')

from agent_util import AgentUtil
from a2c_santorini import SantoriniActor, SantoriniCritic

import gym

env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
     if 'santorini' in env:
          print('Remove {} from registry'.format(env))
          del gym.envs.registration.registry.env_specs[env]
          
import gym_santorini
import json
def tensor_converter(numpy_array): return torch.from_numpy(numpy_array).float()



save_path = ''
file_name_actor = ''
file_name_critic = ''

json_path = "d06_config/setup_config.json"
with open(json_path) as f:
    data = json.load(f)
    save_path =  data['save_path']
    
    file_name_actor = data['save_filename_actor']
    file_name_critic = data['save_filename_critic']

def save_model(torch_object, file_name):
    return torch.save(torch_object.state_dict(), save_path+file_name)

def main_setup():
    
    agent_actor = SantoriniActor()
    agent_critic = SantoriniCritic()
    
    save_model(agent_actor, file_name_actor)
    save_model(agent_critic, file_name_critic)

if __name__ == "__main__":
    main_setup()