import sys
import os
import torch 
import torch.optim as optim
import numpy as np
import matplotlib.pyplot as plt
import json
import copy
sys.path.insert(1, 'd01_agent/')
sys.path.insert(2, 'd00_utils/')
from agent_util import AgentUtil
from a2c_santorini import SantoriniActor, SantoriniCritic
from trueskill import *

setup(backend="mpmath")

import gym

env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
     if 'santorini' in env:
          print('Remove {} from registry'.format(env))
          del gym.envs.registration.registry.env_specs[env]
          
import gym_santorini

# will want to add parallel environments later
# lets try to train on just one instance right now
# sys.path.insert(1, 'd00_utils/')
# from parallel_env import ParallelEnv
# n_train_processes = 3 

def tensor_converter(numpy_array): return torch.from_numpy(numpy_array).float()


load_path_defender = ""
load_defender_actor = ""
load_defender_critic = ""
defender_name = ""

load_path_challenger = ""
load_challenger_actor = ""
load_challenger_critic = ""
challenger_name = ""

save_result_path = ""
game_amount = None

json_path = "d06_config/test_battle_config.json"

with open(json_path) as f:
    data = json.load(f)
    load_path_defender = data["load_path_defender"]
    load_defender_actor = data["load_defender_actor"]
    load_defender_critic = data["load_defender_critic"]
    defender_name = data["defender_name"]
    
    load_path_challenger = data["load_path_challenger"]
    load_challenger_actor = data["load_challenger_actor"]
    load_challenger_critic = data["load_challenger_critic"]
    challenger_name = data["challenger_name"]
    
    save_result_path = data["save_result_path"]
    
    game_amount = data["game_amount"]


bot_rating_path = "d06_config/bot_rating.json"

defender_data = None
defender_init_rating = None
defender_current_rating = None
defender_history = None

challenger_data = None
challenger_init_rating = None
challenger_current_rating = None
challenger_history = None



with open(bot_rating_path) as f:
    data = json.load(f)    
    defender_data = data[defender_name]
    defender_init_rating = defender_data["current_rating"]
    defender_current_rating = copy.deepcopy(defender_init_rating)
    defender_history = defender_data["history"]

    
    
    challenger_data = data[challenger_name]
    challenger_init_rating = challenger_data["current_rating"]
    challenger_current_rating = copy.deepcopy(challenger_init_rating)
    challenger_history = challenger_data["history"]


def load_model( torch_object, load_path):
    return torch_object.load_state_dict(torch.load(load_path))
    
def main_battle():
    defender_rating = Rating(mu=defender_current_rating)
    challenger_rating = Rating(mu=challenger_current_rating)
    print(defender_rating, challenger_rating)
    agent_utility = AgentUtil()
    
    defender_actor = SantoriniActor()
    defender_critic = SantoriniCritic()
    
    challenger_actor = SantoriniActor()
    challenger_critic = SantoriniCritic()
    
    defender_actor.load_state_dict(torch.load(load_path_defender + load_defender_actor))
    defender_critic.load_state_dict(torch.load(load_path_defender + load_defender_critic))
    
    challenger_actor.load_state_dict(torch.load(load_path_challenger + load_challenger_actor))
    challenger_critic.load_state_dict(torch.load(load_path_challenger + load_challenger_critic))
    
    environment =  gym.make('santorini-v0')
    environment.create_board_custom([[6, 8], [16,18]])

    defender_wins = 0
    defender_wins_white = 0
    defender_wins_black = 0
    
    challenger_wins = 0
    challenger_wins_white = 0
    challenger_wins_black = 0
    
    
    halfway_point = game_amount//2
        
    # defender as white
    # challenger as black
    # if game_amount = 100
    # 1,51 
    
    gamma = 0.99
    for i in range(1, halfway_point+1):
        
        current_board_state = environment.reset()
        current_board_state = np.expand_dims(current_board_state, axis = 0)
        current_board_state = np.expand_dims(current_board_state, axis = 0)
        done = False
        while not done:
            if (environment.get_player_turn() == 1):
                
                defender_action_probability = defender_actor.forward(tensor_converter(current_board_state))
                defender_action_probability_reshape = torch.reshape(defender_action_probability, (3,64,5,5))
                
                defender_action_probability_numpy = defender_action_probability_reshape.detach().numpy()
                
                move_dictionary = agent_utility.generate_move_dictionary(environment.check_valid_moves()[1], defender_action_probability_numpy)
                
                softmax_result, move_dictionary = agent_utility.generate_move_softmax(move_dictionary)
                
                action_selected = agent_utility.select_move(softmax_result, move_dictionary)
                
                next_board_state, reward, done = environment.step(action_selected[1])
            
                next_board_state = np.expand_dims(next_board_state, axis=0)
                next_board_state = np.expand_dims(next_board_state, axis=0)
                
                # will probably print this later to figure out if move is good or not
                advantage = reward + (1-done) * gamma * defender_critic(tensor_converter(next_board_state)) - defender_critic(tensor_converter(current_board_state))

                environment.player_swap()
                
                if (len (environment.check_valid_moves()[1]) == 0):
                    done = True
                environment.player_swap()
            
                if done:
                    defender_rating, challenger_rating = rate_1vs1(defender_rating, challenger_rating)
                    defender_wins += 1
                    defender_wins_white += 1
            
                environment.change_player_turn()
                environment.player_swap()
                current_board_state = environment.get_board_state()
                current_board_state = np.expand_dims(current_board_state, axis = 0)
                current_board_state = np.expand_dims(current_board_state, axis = 0)
                
            elif(environment.get_player_turn() == 2):
                challenger_action_probability = challenger_actor.forward(tensor_converter(current_board_state))
                challenger_action_probability_reshape = torch.reshape(challenger_action_probability, (3,64,5,5))
                challenger_action_probability_numpy = challenger_action_probability_reshape.detach().numpy()
                
                move_dictionary = agent_utility.generate_move_dictionary(environment.check_valid_moves()[1], challenger_action_probability_numpy)
                softmax_result, move_dictionary = agent_utility.generate_move_softmax(move_dictionary)
                
                action_selected = agent_utility.select_move(softmax_result, move_dictionary)
                
                next_board_state, reward, done = environment.step(action_selected[1])
                
                next_board_state = np.expand_dims(next_board_state, axis=0)
                next_board_state = np.expand_dims(next_board_state, axis=0)
                
                advantage = reward + (1-done) * gamma * challenger_critic(tensor_converter(next_board_state)) - challenger_critic(tensor_converter(current_board_state))

                environment.player_swap()
                if (len(environment.check_valid_moves()[1]) == 0):
                    done = True
                environment.player_swap()
                
                if done:
                    challenger_rating, defender_rating = rate_1vs1(challenger_rating, defender_rating)
                    challenger_wins += 1
                    challenger_wins_black += 1
                    
                environment.change_player_turn()
                environment.player_swap()
                current_board_state = environment.get_board_state()
                current_board_state = np.expand_dims(current_board_state, axis=0)
                current_board_state = np.expand_dims(current_board_state, axis=0)
        print(defender_rating, challenger_rating)
    # if game_amount = 100
    # 51, 101
    for i in range(halfway_point+1, game_amount+1):
        current_board_state = environment.reset()
        current_board_state = np.expand_dims(current_board_state, axis = 0)
        current_board_state = np.expand_dims(current_board_state, axis = 0)
        done = False
        while not done:
            if (environment.get_player_turn() == 2):
                
                defender_action_probability = defender_actor.forward(tensor_converter(current_board_state))
                defender_action_probability_reshape = torch.reshape(defender_action_probability, (3,64,5,5))
                
                defender_action_probability_numpy = defender_action_probability_reshape.detach().numpy()
                
                move_dictionary = agent_utility.generate_move_dictionary(environment.check_valid_moves()[1], defender_action_probability_numpy)
                
                softmax_result, move_dictionary = agent_utility.generate_move_softmax(move_dictionary)
                
                action_selected = agent_utility.select_move(softmax_result, move_dictionary)
                
                next_board_state, reward, done = environment.step(action_selected[1])
            
                next_board_state = np.expand_dims(next_board_state, axis=0)
                next_board_state = np.expand_dims(next_board_state, axis=0)
                
                # will probably print this later to figure out if move is good or not
                advantage = reward + (1-done) * gamma * defender_critic(tensor_converter(next_board_state)) - defender_critic(tensor_converter(current_board_state))

                environment.player_swap()
                
                if (len (environment.check_valid_moves()[1]) == 0):
                    done = True
                environment.player_swap()
            
                if done:
                    defender_rating, challenger_rating = rate_1vs1(defender_rating, challenger_rating)
                    defender_wins += 1
                    defender_wins_black += 1
            
                environment.change_player_turn()
                environment.player_swap()
                current_board_state = environment.get_board_state()
                current_board_state = np.expand_dims(current_board_state, axis = 0)
                current_board_state = np.expand_dims(current_board_state, axis = 0)
                
            elif(environment.get_player_turn() == 1):
                challenger_action_probability = challenger_actor.forward(tensor_converter(current_board_state))
                challenger_action_probability_reshape = torch.reshape(challenger_action_probability, (3,64,5,5))
                challenger_action_probability_numpy = challenger_action_probability_reshape.detach().numpy()
                
                move_dictionary = agent_utility.generate_move_dictionary(environment.check_valid_moves()[1], challenger_action_probability_numpy)
                softmax_result, move_dictionary = agent_utility.generate_move_softmax(move_dictionary)
                
                action_selected = agent_utility.select_move(softmax_result, move_dictionary)
                
                next_board_state, reward, done = environment.step(action_selected[1])
                
                next_board_state = np.expand_dims(next_board_state, axis=0)
                next_board_state = np.expand_dims(next_board_state, axis=0)
                
                advantage = reward + (1-done) * gamma * challenger_critic(tensor_converter(next_board_state)) - challenger_critic(tensor_converter(current_board_state))

                environment.player_swap()
                if (len(environment.check_valid_moves()[1]) == 0):
                    done = True
                environment.player_swap()
                
                if done:
                    challenger_rating, defender_rating = rate_1vs1(challenger_rating, defender_rating)
                    challenger_wins += 1
                    challenger_wins_white += 1
                    
                environment.change_player_turn()
                environment.player_swap()
                current_board_state = environment.get_board_state()
                current_board_state = np.expand_dims(current_board_state, axis=0)
                current_board_state = np.expand_dims(current_board_state, axis=0)
        print(defender_rating, challenger_rating)

    
    print('defender scores')
    print(defender_wins)
    print(defender_wins_black)
    print(defender_wins_white)
    print(defender_rating)
    
    print('challenger scores')
    print(challenger_wins)
    print(challenger_wins_black)
    print(challenger_wins_white)
    print(challenger_rating)
    
    # need to write a calculate elo function here

if __name__ == "__main__":
    main_battle()

















