# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 22:13:41 2020

@author: Dingie
"""

import torch.multiprocessing as mp
import numpy as np

import sys
import gym

env_dict = gym.envs.registration.registry.env_specs.copy()
for env in env_dict:
     if 'santorini' in env:
          print('Remove {} from registry'.format(env))
          del gym.envs.registration.registry.env_specs[env]
 
# resets the santorini so the re-register error does not pop up
import gym_santorini

def worker(worker_id, master_end, worker_end):
    master_end.close()  # Forbid worker to use the master end for messaging
    # env = SantoriniEnv([[0,0],[0,1]],[[0,2],[0,3]],[[0,0],[0,1]],[[0,2],[0,3]])
    env = gym.make('santorini-v0')
    
    env.seed(worker_id)

    while True:
        cmd, data = worker_end.recv()
        if cmd == 'step':
            ob, reward, done, info = env.step(data)
            if done:
                ob = env.reset()
            worker_end.send((ob, reward, done, info))
        elif cmd == 'reset':
            ob = env.reset()
            worker_end.send(ob)
        elif cmd == 'reset_task':
            ob = env.reset_task()
            worker_end.send(ob)
        elif cmd == 'close':
            worker_end.close()
            break
        elif cmd == 'get_spaces':
            worker_end.send((env.observation_space, env.action_space))
        else:
            raise NotImplementedError
            
class ParallelEnv:
    def __init__(self, n_train_processes):
        print('hi guy')
        self.nenvs = n_train_processes
        self.waiting = False
        self.closed = False
        self.workers = list()
        # mp.Pipe() creates a master and worker ends.
        # zip(*arg) is unzipping the values and setting it them to aster_ends and worker_ends, respectively.
        # master_ends => sends messages/actions
        # worker_ends => receives messages/ performs actions
        master_ends, worker_ends = zip(*[mp.Pipe() for _ in range(self.nenvs)])
        self.master_ends, self.worker_ends = master_ends, worker_ends
        # zip file combines each master/worker pair
        # enumerate adds a counter to each pair in the sequence to pass to the work function 
        # continue here need to figure out whwat worker_end.close() and master_end.close is doing
        for worker_id, (master_end, worker_end) in enumerate(zip(master_ends, worker_ends)):
            p = mp.Process(target=worker,
                           args=(worker_id, master_end, worker_end))
            p.daemon = True
            # invokes the run method which will start the worker function
            p.start()
            self.workers.append(p)

        # Forbid master to use the worker end for messaging
        for worker_end in worker_ends:
            # not sure why worker_end.close() is called. Seems that you would want communication between master and worker
            worker_end.close()
            

    def step_async(self, actions):
        for master_end, action in zip(self.master_ends, actions):
            master_end.send(('step', action))
        self.waiting = True

    def step_wait(self):
        results = [master_end.recv() for master_end in self.master_ends]
        self.waiting = False
        obs, rews, dones, infos = zip(*results)
        return np.stack(obs), np.stack(rews), np.stack(dones), infos

    def reset(self):
        for master_end in self.master_ends:
            master_end.send(('reset', None))
        return np.stack([master_end.recv() for master_end in self.master_ends])

    def step(self, actions):
        self.step_async(actions)
        return self.step_wait()

    def close(self):  # For clean up resources
        if self.closed:
            return
        if self.waiting:
            [master_end.recv() for master_end in self.master_ends]
        for master_end in self.master_ends:
            master_end.send(('close', None))
        for worker in self.workers:
            worker.join()
            self.closed = True
