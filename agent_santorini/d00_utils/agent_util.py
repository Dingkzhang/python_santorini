
import numpy as np

import torch

class AgentUtil:
    
    def __init__(self):
        return None
    
    def generate_move_dictionary(self, valid_moves, output_layer):
        move_dictionary = []
        
        #https://www.youtube.com/watch?v=EPIUxGOynE0
        
        for move in valid_moves:
            move_soft_max_value = output_layer[move[0]][move[1]][move[2]][move[3]]
            
            move_dictionary.append([move_soft_max_value, move])
            
        move_dictionary.sort()
        return move_dictionary
    
    def generate_move_softmax(self, move_dictionary):        
        softmax_array = []        
        for move in move_dictionary:
            softmax_array.append(move[0])            
        softmax_result = self.softmax(softmax_array)

        # probably unnecessary but im going to add it here just in case
        for i in range(len(move_dictionary)):
            move_dictionary[i][0] = softmax_result[i]
        return softmax_result, move_dictionary

    def select_move(self, softmax_result, move_dictionary):
        # error in which np random choice says that first input must be 1-dimensional >:(
        action_selected = np.random.choice(list(range(len(softmax_result))), p = softmax_result)
        return move_dictionary[action_selected]
    
    def softmax(self, input_values):
        return np.exp(input_values) / np.sum(np.exp(input_values), axis=0)