from gym.envs.registration import register

register(
    id='santorini-v0',
    entry_point='gym_santorini.d02_environment:SantoriniEnv',
)
