# -*- coding: utf-8 -*-
"""
Created on Sun May 17 15:04:54 2020

@author: Dingie
"""

import gym
from gym import spaces
import numpy as np
import random as rd
import sys

from gym import error, spaces, utils
from gym.utils import seeding

# sys.path.insert(1, '../d00_utils/')

# from santoriniValid import SantoriniValid

# this function is used to reload the class so I don't have to restart blender everytime >:( fuck you blender!!! jk you are awesome


# The board state will be a 6x5x5 board
# will need to do 3-D filtering to reduce and learn the pattern to solve for the problem
class SantoriniEnv(gym.Env):

    # first element is y-axis, second element is x axis. y-axis positive direction is down x-axis positive direction is right.
    player_actions = [[-1,0], [-1,1], [0,1], [1,1], [1,0], [1,-1], [0,-1], [-1, -1]]
    # there are only 25 starting positions in the game
    start_positions = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24]
    # encodes the moves in a series of array values
    init_move_logger = []
    move_logger = []
    # what the board state looks like at the start/reset of the game
    init_board_state = []
    # current board state of the game
    board_state = []
    metadata = {'render.modes': ['human']}    

    
    def __init__(self):
        super(SantoriniEnv, self).__init__()
 
    # retire this function once I finish def create_board_init(self):
    def create_board(self, player1_position, player2_position):
        self.init_p1_position = player1_position
        self.init_p2_position = player2_position
        self.player1_position = player1_position[:]
        self.player2_position = player2_position[:]
        self.reset()
    
    # assumes that we want to generate a random initial game state
    # depth will be added much later? depth represents how many moves are made
    def create_board_random(self, depth= 0):
        start_positions = rd.sample(range(0,25),4)
        configured_start_position = [start_positions[:2], start_positions[2:]]
        
        self.init_move_logger.append(start_positions[:2])
        self.init_move_logger.append(start_positions[2:])
        
        self.move_logger = self.init_move_logger[:]
        
        
        self.set_starting_position(configured_start_position)
        self.set_starting_board_state()
        self.reset()
        return None
    
    def create_board_custom(self, start_array):
        self.init_move_logger = start_array
        
        self.move_logger = self.init_move_logger[:]
        self.start_array = start_array
        self.set_starting_position(start_array)
        self.set_starting_board_state()
    
    def set_starting_board_state(self):
        self.init_board_state = np.zeros((6,5,5))        
        self.init_board_state[0,self.init_p1_position[0][0],self.init_p1_position[0][1]] = 1
        self.init_board_state[0,self.init_p1_position[1][0],self.init_p1_position[1][1]] = 1
        self.init_board_state[1,self.init_p2_position[0][0],self.init_p2_position[0][1]] = 1
        self.init_board_state[1,self.init_p2_position[1][0],self.init_p2_position[1][1]] = 1
        self.board_state = self.init_board_state[:]
        for move in self.init_move_logger[2:]:
            y_axis = move[1] // 5
            x_axis = move[1] % 5
            placeholder_index = 0 
            current_move = [placeholder_index, move[0], y_axis, x_axis]
            self.step(current_move, True)
            self.player_swap()
        self.init_board_state = self.board_state[:]
        
    def set_starting_position(self, start_array):
        player1_array = []
        player2_array = []
        self.start_array = start_array
        for p1_value in start_array[0]:
            y_axis = p1_value // 5
            x_axis = p1_value % 5
            player1_array.append([y_axis,x_axis])

        for p2_value in start_array[1]:
            y_axis = p2_value // 5
            x_axis = p2_value % 5
            player2_array.append([y_axis,x_axis])

        self.player_turn = 1
        self.init_p1_position = player1_array[:]
        self.init_p2_position = player2_array[:]
        self.player1_position = player1_array
        self.player2_position = player2_array
        
    def add_to_move_logger(self, piece_action, current_y, current_x):
        piece_position = current_y * 5 + current_x
        self.move_logger.append([piece_action,piece_position])

    def move_logger_decoder(self):
        return None
    
    def check_valid_moves(self):
        return self.check_actions_valid(self.board_state)
    
    def print_move_logger(self):
        print(self.move_logger)
        
    def get_move_logger(self):
        return self.move_logger
    
    def get_player_turn(self):
        return self.player_turn
    
    
    def change_player_turn(self):
        if (self.player_turn == 1):
            self.player_turn = 2
            
        elif(self.player_turn == 2):
            self.player_turn = 1
    
    def print_player_position(self):
        print('Player 1 current position')
        print(self.player1_position)
        print('Player 2 current position')
        print(self.player2_position)
    
    def print_player_init_position(self):
        print('Player 1 init position')
        print(self.init_p1_position)
        print('Player 2 init position')
        print(self.init_p2_position)

    # 4 values:, level, move_command, y_location, x_location
    def step(self, action, init_setup=False):
        move_index = action[1] // 8
        build_index = action[1] % 8
        
        current_y = action[2]
        current_x = action[3]
        new_y = action[2] + self.player_actions[move_index][0]
        new_x = action[3] + self.player_actions[move_index][1]
        
        if (not init_setup):    
            self.add_to_move_logger(action[1], action[2], action[3])
        
        if (self.player_turn == 1):
            for i in range(2):
                if self.player1_position[i][0] == current_y and self.player1_position[i][1] == current_x:
                    self.player1_position.pop(i)
                    break
            self.player1_position.append([new_y, new_x])

        elif (self.player_turn == 2):            
            for i in range(2):
                if self.player2_position[i][0] == current_y and self.player2_position[i][1] == current_x:
                    self.player2_position.pop(i)
                    break
            self.player2_position.append([new_y, new_x])         

        build_y = action[2] + self.player_actions[move_index][0] + self.player_actions[build_index][0]
        build_x = action[3] + self.player_actions[move_index][1] + self.player_actions[build_index][1]
        
        building_state = np.sum([self.board_state[2], self.board_state[3], self.board_state[4], self.board_state[5]] , axis=0)
        current_building_level = building_state[build_y,build_x]
        build_level_index = int(current_building_level) + 2
        
        self.board_state[0, current_y, current_x] = 0
        self.board_state[0, new_y, new_x] = 1
        self.board_state[build_level_index, build_y, build_x] = 1
        
        is_game_over, reward = self.check_game_over()
        # print('game over: ', is_game_over)
        

        return self.board_state, reward, is_game_over

    def step_blender(self,action):    
        return None
    
    def reset(self):       
        self.player_turn = 1
        # self.board_state = np.zeros((6,5,5))        
        # self.board_state[0,self.init_p1_position[0][0],self.init_p1_position[0][1]] = 1
        # self.board_state[0,self.init_p1_position[1][0],self.init_p1_position[1][1]] = 1
        # self.board_state[1,self.init_p2_position[0][0],self.init_p2_position[0][1]] = 1
        # self.board_state[1,self.init_p2_position[1][0],self.init_p2_position[1][1]] = 1
        self.set_starting_position(self.start_array)
        self.set_starting_board_state()

        
        self.move_logger = self.init_move_logger[:]
        
        return self.board_state
        
    def render(self, mode='human', close=False):
        print(self.board_state)

    def print_board_state(self):
        print(self.board_state)

    def get_board_state(self):
        return self.board_state

    def update_player_2(self):
        return None

    def player_swap(self):
        player_state= np.array(self.board_state[0])
        player_state_2 = np.array(self.board_state[1])
        
        self.board_state[0] = player_state_2
        self.board_state[1] = player_state

    # you have the current position of the board and players now with the variables:
    # self.player1_position and self.player2_position
    # user the values of those to determine if the position of any of the values match the 4th array
    def check_game_over(self):
        is_game_over = False
        reward = 0 
        for i in range(2):
            if (self.board_state[4][self.player1_position[i][0]][self.player1_position[i][1]] == 1):
                is_game_over = True
                reward = 1
                break
            elif(self.board_state[4][self.player2_position[i][0]][self.player2_position[i][1]] == 1):
                is_game_over = True
                reward = -1
                break
            
            elif(len(self.check_actions_valid(self.board_state)) == 0):
                is_game_over = True
                reward = 0
        return is_game_over, reward
    
    #ends the simulation
    def close(self):
        return None
    
    # return result should be [0,[0,1],[0,1]] player, movement, build
    def check_actions_valid(self, board_state):
        
        #should return the available actions
        
        # 6x5x5
        # 3x64x5x5 with 1s for available and -1 for not available
        # if all -1 then terminate game
        
        building_state = np.sum([board_state[2], board_state[3], board_state[4], board_state[5]], axis=0)
        player_state = np.sum([board_state[0], board_state[1]], axis=0)
        action_state = np.zeros((3,64,5,5))
        #player_level, tower_row (y), tile (x)
        player_piece_location = self.player_location(board_state, building_state)
#        print(player_piece_location)
        
        # this will need to be modified later I do not like this :()
        if  (player_piece_location[0][0] == 3 or player_piece_location[1][0] == 3):
            return [], [], [], []
        action_state = self.check_player(player_piece_location, board_state, building_state, action_state)
        valid_moves_remaining, action_state = self.check_movement(player_piece_location, board_state, building_state, player_state, action_state)

        valid_moves_remaining = self.check_build(valid_moves_remaining, player_piece_location, board_state, building_state, player_state, action_state)
        
        new_action_state = self.valid_move_board_state(valid_moves_remaining)
        
        
        return new_action_state, valid_moves_remaining, player_state, building_state
    
    
    
    def player_location(self, board_state, building_state):
        
        num_player_checked = 0
        player_piece_location = []
        for tower_row in range(0,5):
            for tile in range(0,5):
                if (board_state[0][tower_row][tile] == 1):
#                    print('found player')
                    player_level = building_state[tower_row][tile]
                    player_piece_location.append([int(player_level), tower_row, tile])
                    
                    num_player_checked += 1
        
                if (num_player_checked == 2):
                    break
            if (num_player_checked == 2):
                break
        
        return player_piece_location
    
     
    def check_player(self, player_piece_location, board_state, building_state, action_state):
        num_player_checked = 0
        # should be 128 actions remaining
        action_checker = 0

        for player_piece in range(len(player_piece_location)):
#            print(player_piece)
            piece = player_piece_location[player_piece]
#            print(piece)
#            print(piece)
            for actions in range(0,64):
            
                action_state[piece[0], actions, piece[1], piece[2]] = 1
                action_checker += 1
        if (action_checker == 128):
            where_0 = np.where(action_state == 0)
            action_state[where_0] = -1
                            
#        print("inside check player")
#        print("Checking board state")
#        print(board_state)
#        
#        print("Checking building state")
#        print(building_state)
#        
#        print("checking action state")
#        print(action_state)
#        print("checking action_checker")
#        print(action_checker)

        return action_state
    
    def check_movement(self, player_piece_location, board_state, building_state, player_state, action_state):
        available_move = 0
        valid_moves_remaining = [] 
        for player_piece in player_piece_location:
            move_counter = 0
            valid_action = 0

            for action in self.player_actions:
                new_position = [player_piece[1] + action[0], player_piece[2] + action[1]]
#                print(new_position)
                
                # border checker
                if (-1 in new_position or 5 in new_position):
                    valid_action = -1
                # player checker
                elif (player_state[new_position[0]][new_position[1]] == 1):
                    valid_action =  -1
                # building checker    
                elif(abs(player_piece[0] - building_state[new_position[0]][new_position[1]]) >= 2):
                    valid_action = -1
                else:
                    valid_action = 1
                    available_move += 1
                for direction in range(8):
                    action_state[player_piece[0], move_counter, player_piece[1], player_piece[2]] = valid_action
                    if (valid_action == 1):
                        valid_moves_remaining.append([player_piece[0], move_counter, player_piece[1], player_piece[2]])
                    move_counter += 1
#            print(action_state)
#            print(available_move)
#            
#        print(player_piece_location)
#        print(player_state)
#        print(building_state)
#        print(valid_moves_remaining)
        return valid_moves_remaining, action_state
    
    def check_build(self, valid_moves_remaining, player_piece_location, board_state, building_state, player_state, action_state):
        
#        print('inside check_build')
#        print(player_piece_location)
#        print(board_state)
#        print(building_state)
#        print(player_state)
#        print(valid_moves_remaining)
        invalid_moves = []
        for valid_move in valid_moves_remaining:
#            print(valid_move)
            valid_action = 0
            
#            print(valid_move[1] // 8)
#            print(valid_move[1] % 8)
            move_action_index = valid_move[1] // 8
            build_action_index = valid_move[1] % 8 
            
            new_build_position = [valid_move[2] + self.player_actions[move_action_index][0] + self.player_actions[build_action_index][0], 
                                    valid_move[3] + self.player_actions[move_action_index][1] + self.player_actions[build_action_index][1]]
            

            temp_player_state = player_state
            temp_player_state[valid_move[2]][valid_move[3]] = 0
            temp_player_state[valid_move[2] + self.player_actions[move_action_index][0]][valid_move[3] + self.player_actions[move_action_index][1]] = 1
#            print(temp_player_state)
            
            if (-1 in new_build_position or 5 in new_build_position):
                valid_action = -1
                invalid_moves.append(valid_move)
            elif(temp_player_state[new_build_position[0]][new_build_position[1]] == 1):
                valid_action = -1
                invalid_moves.append(valid_move)
            elif(building_state[new_build_position[0]][new_build_position[1]] > 3):
                valid_action = -1
                invalid_moves.append(valid_move)
                
            temp_player_state[valid_move[2]][valid_move[3]] = 1
            temp_player_state[valid_move[2] + self.player_actions[move_action_index][0]][valid_move[3] + self.player_actions[move_action_index][1]] = 0
        for invalid_move in invalid_moves:
            valid_moves_remaining.remove(invalid_move)
#        print('results')
#        print(valid_moves_remaining)
#        print(player_state)
#        print(building_state)
#        print(action_state)
        return valid_moves_remaining
    def valid_move_board_state(self, valid_moves_remaining):

        
        new_action_state = np.zeros((3,64,5,5))
        for valid_move in valid_moves_remaining:
            new_action_state[valid_move[0],valid_move[1],valid_move[2],valid_move[3]] = 1

#        print(new_action_state)
#        print('valid_move_board_state')
#        print(valid_moves_remaining)
        return new_action_state
    

# test = SantoriniEnv()
# test.create_board([[0,0],[0,1]],[[0,2],[0,3]])

# # way to move if we were going to use a neural network for each player
# test_action= [1,36,0,0]
# test.step(test_action)

# test.player_swap()
# _, valid_moves, _, _ = test.check_valid_moves()

# print(valid_moves)

# test_action = [0,25,0,2]
# test.step(test_action)
# test.player_swap()

# test.print_board_state()

# test.print_player_init_position()
# test.print_player_position()

# test.reset()

# test.print_player_init_position()
# test.print_player_position()