# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 18:00:10 2020

@author: Dingie
"""

import numpy as np
import torch
import torch.nn as nn


class ActorCritic(nn.Module):
    
    # input -> 6x5x5
    # conv -> 3x3x3 (20)
    # -> 4x3x3 (20)
    # leaky relu
    # padding of 4x3x3 with 0,1,1 -> 4x4x4 (20)
    # max pooling -> 4x4x4 (20)
    # 2x2x2 (20)
    # fully connected layer -> 160
    # flatten 
    # relu -> 128
    # batch normalize -> 128
    # dropout -> 128 trying it out:) ??? 
    # output -> 4800
    
    # code is based off of this project
    # https://github.com/miki998/3d_convolution_neural_net_MNET/blob/master/pytorch_model.py
    def __init__(self):
        super(ActorCritic, self).__init__()
        self.conv_layer = self.conv_layer_set(1, 20)
        self.fc1 = nn.Linear(2**3*20, 240)        
        self.relu = nn.LeakyReLU()
        self.batch = nn.BatchNorm1d()
        self.drop = nn.Dropout(p=0.15)
        self.fc_action = nn.Linear(240,4800)
        self.fc_value = nn.Linear(240,1)

    def conv_layer_set(self, in_c, out_c):
        conv_layer = nn.Sequential(
            
        nn.Conv3d(in_c, out_c, kernel_size=(3,3,3)),
        nn.LeakyReLU(),
        nn.MaxPool3d((2,2,2), padding=(0,1,1))
        )
        return conv_layer
    
    def forward(self, input_value):
        out = self.conv_layer(input_value)
        out = out.view(out.size(0),  -1)
        out = self.fc1(out)
        out = self.relu(out)
        out = self.batch(out)
        out = self.drop(out)
        return out

    def forward_action(self, input_value):
        out = self.fc_action(input_value)
        action_prob = nn.Softmax(out)
        return action_prob
    
    def forward_value(self, input_value):
        return self.fc_value(input_value)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    